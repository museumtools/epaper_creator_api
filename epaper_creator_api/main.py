from enum import Enum
# from typing import Optional
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import mw
import wiki2img


origins = [
    "http://192.168.100.100",
    "http://localhost",
    "http://localhost:8080",
    "http://127.0.0.1",
]

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get('/')
def say_hello():
    return {'hello': 'world'}


@app.get('/testwiki')
def test_wiki():
    site_ = mw.wiki_site(host='museumtools.artserver.org', path='/wiki/',
                         scheme='https')
    return {
        'wiki': f'{site_.scheme}://{site_.host}{site_.path}',
        'wiki version': str(site_.version)
    }


class ArtifactPagename(str, Enum):
    amsterdam_als_centrum = 'Amsterdam als centrum van de wereldhandel'
    de_suikerplantage = 'De suikerplantage Waterlant in Suriname'
    de_terugkomst = 'De terugkomst in Amsterdam van de tweede expeditie naar ' \
                    'Oost-Indië'
    gezicht = 'Gezicht op Amsterdam in vogelvlucht'


class EpaperDevice(str, Enum):
    seven_point_eight = '7_8'
    thirteen = '13'


@app.get('/artifact_stories/{artifact_pagename}')
def perform_ask_query(artifact_pagename: ArtifactPagename,
                      device: EpaperDevice):
    if device not in wiki2img.epaper_devices.keys():
        raise HTTPException(
            status_code=400,
            detail=f"Supplied device size is not found. Choose from list: "
                   f"{wiki2img.epaper_devices.keys()}")
    site_ = mw.wiki_site(host='museumtools.artserver.org', path='/wiki/',
                         scheme='https')
    askquery = f'[[{artifact_pagename}]]||[[StoryOf::{artifact_pagename}]]||' \
               f'[[Visual StoryOf::{artifact_pagename}]]' \
               f'|?StorySelectionOrder|?Category' \
               f'|?Title|?Creator|?Date|?Source|?Subject' \
               f'|?Has image' \
               f'|?StoryType|?Language' \
               f'|mainlabel=StoryPage|link=none' \
               f'|sort=StorySelectionOrder'
    results = mw.ask(query=askquery)
    results_objects = wiki2img.list_dict2list_objects(list_dict=results,
                                                      device=device)
    # import pdb; pdb.set_trace()

    return results

# for tests
#  results_objects[0].__dict__
# {'mw_pagedata': {'page': 'Amsterdam als centrum van de wereldhandel', 'StorySelectionOrder': 0, 'Category': 'Category:Artifact', 'Title': 'Amsterdam als centrum van de wereldhandel', 'Creator': 'Pieter Isaacsz', 'Date': datetime.datetime(1606, 1, 1, 0, 19, 32), 'Source': "Amsterdam Museum's Permanent Gallery Paintings", 'Has image': 'https://museumtools.artserver.org//wiki/images/a/a5/Amsterdam_als_centrum_van_de_wereldhandel.jpg'}}


# results var value (for mocking)
# [{'page': 'Amsterdam als centrum van de wereldhandel',  'StorySelectionOrder': 0, 'Category': 'Category:Artifact', 'Title': 'Amsterdam als centrum van de wereldhandel', 'Creator': 'Pieter Isaacsz', 'Date': datetime.datetime(1606, 1, 1, 0, 19, 32), 'Source': "Amsterdam Museum's Permanent Gallery Paintings", 'Has image': 'File:Amsterdam als centrum van de wereldhandel.jpg'}, {'page': 'Test Story', 'StorySelectionOrder': 1, 'Category': 'Category:Story', 'Title': 'Test Story', 'Creator': 'Amsterdam Museum', 'Language': 'en'}, {'page': 'Global City', 'StorySelectionOrder': 2, 'Category': 'Category:Story', 'Title': 'Global City', 'Creator': 'Amsterdam Museum', 'StoryType': 'City Visual Story', 'Language': 'en'}, {'page': 'Wereldstad', 'StorySelectionOrder': 3, 'Category': 'Category:Story', 'Title': 'Wereldstad', 'Creator': 'Amsterdam Museum', 'StoryType': 'City Visual Story', 'Language': 'nl'}, {'page': 'Test Visual Story', 'StorySelectionOrder': 4, 'Category': 'Category:Visual Story', 'Title': 'Test Visual Story', 'Creator': 'Unknown', 'Date': datetime.datetime(2020, 12, 4, 1, 0), 'Has image': 'File:MvdK.jpg'}]
