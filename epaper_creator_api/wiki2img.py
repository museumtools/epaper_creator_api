from typing import List, OrderedDict, ClassVar
import datetime
import os
from pathlib import Path
from jinja2 import (FileSystemLoader,
                    Environment)
from weasyprint import HTML, CSS
from PIL import Image, ImageEnhance
import mw

epaper_devices = {'7_8': {'dimensions': '1404x1872',
                          'stylesheet': 'page_78inch.css'},
                  '13': {'dimensions': '1404x1872',
                         'stylesheet': 'page_78inch.css'},
                  }


class PageContent():
    def __init__(self, mw_pagedata: OrderedDict, device: str):
        self.mw_pagedata = mw_pagedata
        self.wikifile2url()
        self.stylesheet = epaper_devices[device]['stylesheet']
        self.page_template = self.load_template(
            template='epaper_page.html.jinja'
        )
        self.page_html = self.page_template.render(
            page_properties=self.mw_pagedata,
            css_file=self.stylesheet)

        png_fn = f'{self.mw_pagedata["StorySelectionOrder"]:02}_' \
                 f'{self.mw_pagedata["Title"].replace(" ","_")}.png'

        png_path = f'{Path(__file__).parent / "outputs" / png_fn}'
        self.savecontent(target_fn=png_path.replace('.png', '.html'),
                         content=self.page_html)   # temporary storage
        self.html2png(
            target_fn=Path(png_path),  # replace
            html_code=self.page_html,
            css_file=f"{Path(__file__).parent / 'templates' / self.stylesheet}")
        self.png2data(source_fn=png_path,
                      dest_fn=png_path.replace('.png', '.data'), brightness_level=2)

    def wikifile2url(self):
        if 'Has image' in self.mw_pagedata.keys():
            self.mw_pagedata['Has image'] = mw.getfileurl(
                filepage=self.mw_pagedata['Has image'])

    def load_template(self, template: str):
        f_loader = FileSystemLoader(Path(__file__).parent / 'templates')
        env = Environment(loader=f_loader)
        template_obj = env.get_template(template)
        return template_obj

    def html2png(self, target_fn: ClassVar, html_code: str, css_file: str):
        if target_fn.parent.is_dir() is False:
            os.mkdir(target_fn.parent)
        html = HTML(string=html_code)
        html.write_png(
            target=str(target_fn),
            stylesheets=[CSS(filename=css_file)],
            resolution=300)

    def png2data(self, source_fn: str, dest_fn: str,  brightness_level: int):
        with Image.open(source_fn) as im:
            brightness = ImageEnhance.Brightness(im)
            im = brightness.enhance(brightness_level)  # 2
            # print(dimensions_list[0], int(dimensions_list[1]))
            # im = im.resize((1404, 1872))
            # im = im.resize((int(dimensions_list[0]), int(dimensions_list[1])))
            blackAndWhiteImage = im.convert("1")
            # blackAndWhiteImage.show()
            quantizedImage = im.quantize(colors=2, method=2)
            # print(quantizedImage.format, quantizedImage.size,
            # blackAndWhiteImage.mode)
            blackAndWhiteImage.save(dest_fn, "BMP")

    def savecontent(self, target_fn: ClassVar, content: str):
        target_fn_path = Path(target_fn)
        target_fn_parent = target_fn_path.parent
        if target_fn_parent.is_dir() is False:
            os.mkdir(target_fn_parent)
        with open(target_fn, 'w') as fn:
            fn.write(content)


def list_dict2list_objects(list_dict: List, device: str) -> List:
    list_objects = []
    for page_dict in list_dict:
        list_objects.append(PageContent(mw_pagedata=page_dict, device=device))
    return list_objects


if __name__ == '__main__':
    mw.wiki_site(host='museumtools.artserver.org', path='/wiki/',
                 scheme='https')
    ask_results = [{
      'page': 'Amsterdam als centrum van de wereldhandel',
      'StorySelectionOrder': 0, 'Category': 'Category:Artifact',
      'Title': 'Amsterdam als centrum van de wereldhandel',
      'Creator': 'Pieter Isaacsz',
      'Date': datetime.datetime(1606, 1, 1, 0, 19, 32),
      'Source': "Amsterdam Museum's Permanent Gallery Paintings",
      'Has image': 'File:Amsterdam als centrum van de wereldhandel.jpg'},
     {'page': 'Test Story', 'StorySelectionOrder': 1,
      'Category': 'Category:Story', 'Title': 'Test Story',
      'Creator': 'Amsterdam Museum', 'Language': 'en'},
     {'page': 'Global City', 'StorySelectionOrder': 2,
      'Category': 'Category:Story', 'Title': 'Global City',
      'Creator': 'Amsterdam Museum', 'StoryType': 'City Visual Story',
      'Language': 'en'}, {'page': 'Wereldstad', 'StorySelectionOrder': 3,
                          'Category': 'Category:Story', 'Title': 'Wereldstad',
                          'Creator': 'Amsterdam Museum',
                          'StoryType': 'City Visual Story', 'Language': 'nl'},
     {'page': 'Test Visual Story', 'StorySelectionOrder': 4,
      'Category': 'Category:Visual Story', 'Title': 'Test Visual Story',
      'Creator': 'Unknown', 'Date': datetime.datetime(2020, 12, 4, 1, 0),
      'Has image': 'File:MvdK.jpg'}]
    results_objects = list_dict2list_objects(list_dict=ask_results,
                                             device='7_8')
    results_objects_0 = results_objects[0]
    print(results_objects_0.__dict__)

