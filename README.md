# FastAPI 

## Development
### Run development mode
`cd epaper_creator_api/`

`uvicorn main:app --reload`

Available http://127.0.0.1:8000

### Run wiki2img.py

`$ cd epaper_creator_api/`

`$ python wiki2img.py`

Results will be save in epaper_creator_api/outputs/

### API Docs
 http://127.0.0.1:8000/docs


## Production: deploy with docker & docker-compose

Build image and bring container up:
`docker-compose up --build`

API will be available at http://0.0.0.0:7979/

API documentation at http://0.0.0.0:7979/docs