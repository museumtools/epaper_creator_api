FROM python:3.7-alpine

RUN apk --update --upgrade add gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev

EXPOSE 80

COPY ./epaper_creator_api /app
COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt
WORKDIR /
ENV PYTHONPATH app
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]